def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(numbers)
  sum = 0
  numbers.each { |element| sum += element }
  sum
end

def multiply(numbers)
  product = 1
  numbers.each { |element| product *= element }
  product
end

def power(num1, num2)
  product = 1
  num2.times { product *= num1 }
  product
end

def factorial(number)
  product = 1
  1.upto(number) do | current_num |
    product *= current_num
  end
  product
end
