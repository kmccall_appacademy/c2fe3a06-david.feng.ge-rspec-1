def translate(string)
  pig_latin_words = []
  string.split.each do |word|
    pig_latin_words << translate_a_word(word)
  end
  pig_latin_words.join(" ")
end


def translate_a_word(word)
  if vowel?(word[0])
    return "#{word}ay"
  else
    vowel_index = first_vowel_index(word)
    return "#{word[vowel_index..-1]}#{word[0..vowel_index-1]}ay"

  end
end

def vowel?(letter)
  %w(a e i o u A E I O U).include?(letter)
  # %w(foo bar) is a shortcut for ["foo", "bar"]
end

def first_vowel_index(word)
  0.upto(word.length).each do |index|
    if index >=1 && word[index] == "u" && word[index-1] == "q"
      next
    end
    return index if vowel?(word[index])
  end
end
