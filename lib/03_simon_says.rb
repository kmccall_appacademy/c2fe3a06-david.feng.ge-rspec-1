def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, time = 2)
  result = ""
  1.upto(time) do |index|
    if index < time
      result += "#{string} "
    else
      result += "#{string}"
    end
  end

  result
end

def start_of_word(word, num = 1)
  word[0, num]
end

def first_word(sentence)
  words = sentence.split(" ")
  words[0]
end

def titleize(original)
  words = original.split(" ")
  new_words = words.map.with_index do |word, index|
    if ["the", "a", "an", "of", "for", "and", "or", "over"].include?(word) && index != 0
      word
    else
      word.capitalize
    end
  end
  new_words.join(" ")
end
